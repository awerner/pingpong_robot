#include <ros/ros.h>
#include <cmath>
#include <iostream>
#include "pingpong_robot/Num.h"
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <cmath>
#include <mutex>
#include <franka/exception.h>
#include <franka/robot.h>
#include <franka/gripper.h>
#include "examples_common.h"
#include <thread>
#include <math.h> 

// variables to assign the ball origin coordinates to
double x_last = 0.0 ;
double y_last = 0.0 ;
double z_threshold; //threshold over origin 10cm

double x_desired = 0.0 ;
double y_desired = 0.0 ;

bool first_callback_loop = true ;
bool first_call_for_motion_function = true ; 
bool first_call_for_hit_ball_function = true ;
bool hit_ball_now = false;
bool time_since_last_message = false ;
bool keep_running;
bool start_juggling = false;

std::mutex mymutex;
std::mutex xy_desired_mutex;


void go_to_initial_position(franka::Robot & robot)
{
  // going to an initial position to start from it
	std::array<double, 7> point_2 = {{M_PI_2, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI, -M_PI_4}};
  MotionGenerator initial_position(0.4, point_2);
  robot.control(initial_position);
}

int gripper_grip (franka::Gripper & gripper){
  gripper.homing();
	double grasping_width = 0.024;
	// Check for the maximum grasping width.
  franka::GripperState gripper_state = gripper.readOnce();
  if (gripper_state.max_width < grasping_width) {
    std::cout << "Object is too large for the current fingers on the gripper." << std::endl;
    return -1;
  }
  // Grasp the object.
  if (!gripper.grasp(grasping_width, 0.1, 60)) {
    std::cout << "Failed to grasp object." << std::endl;
   	return -1;
  }
}
void gripper_releas(franka::Gripper & gripper){
  std::cout << "Grasped object, will release it now." << std::endl;
  gripper.stop();
}

int motion (franka::Robot & robot){
  try{

    double time_now_x = 0.0;
    double time_now_y = 0.0;
    double time_now_z = 0.0;
    double time_required_x;
    double time_required_y;
    std::array<double, 16> initial_pose; 
    std::array<double, 16> new_pose;
    double angle_x ;
    double angle_y ;
    double angle_z ;
    double x_desired_filtered = 0;
    double y_desired_filtered = 0;
    double delta_z ;
    bool new_message_x ;
    bool new_message_y ;
    
    robot.control([&new_pose , &initial_pose, &x_desired_filtered, &y_desired_filtered,
                    &time_required_x, &time_required_y,
                    &angle_x, &angle_y, &angle_z , &delta_z,
                    &new_message_x, &new_message_y, &time_now_x, &time_now_y, &time_now_z ]
                      (const franka::RobotState& robot_state,
                         franka::Duration period) -> franka::CartesianPose {
      time_now_x += period.toSec();
      time_now_y += period.toSec();

      if (first_call_for_motion_function) {
        initial_pose = robot_state.O_T_EE_c;
        new_pose = initial_pose;
        first_call_for_motion_function = false ; 
      }

      {
        std::unique_lock<std::mutex> lock(xy_desired_mutex);
        if (x_desired_filtered != x_desired && fabs(x_desired) >= 0.02){
          x_desired_filtered = x_desired ;
          new_message_x = true;
        }
        if (y_desired_filtered != y_desired && fabs(y_desired) >= 0.02){
          y_desired_filtered = y_desired;
          new_message_y = true;
        }
      }

      if (new_message_x){
        time_now_x = 0;
        time_required_x = fabs(x_desired_filtered)* 17 ; // speed of 1/17 m/s 
        new_message_x = false ;
	      initial_pose[12] = new_pose[12];
      }
      if (new_message_y){
	      time_now_y= 0;
        time_required_y = fabs(y_desired_filtered)* 17 ;
        new_message_y = false ;
        initial_pose[13] = new_pose[13];
      }   
      if (time_now_x <= time_required_x  && x_desired_filtered != 0.0){
        angle_x = M_PI / 4 * (1 - std::cos(M_PI / time_required_x * time_now_x));
        new_pose[12] = initial_pose[12] + x_desired_filtered * std::sin(angle_x);
      }
      if (time_now_y <= time_required_y  && y_desired_filtered != 0.0){
        angle_y = M_PI / 4 * (1 - std::cos(M_PI / time_required_y * time_now_y));
        new_pose[13] = initial_pose[13] + y_desired_filtered * std::sin(angle_y);
        // std::cout << "new: " <<new_pose[13]<<", initial: " << initial_pose[13]<< ", time = " <<
        //       time_now_y << " out of " << time_required_y << ",  " << y_desired_filtered << std::endl;        
      }
      if (hit_ball_now){

	      time_now_z += period.toSec();
        angle_z = M_PI / 4 * (1 - std::cos(M_PI / 0.6 * time_now_z));
        delta_z = 0.15 * (std::cos(angle_z) - 1);
        new_pose[14] = delta_z + initial_pose[14] ;
	      // std::cout << "new: " <<new_pose[14]<<", initial: " << initial_pose[14]<< ", time = " <<
        //       time_now_z << " out of " << 2 << ",  " << delta_z << std::endl;        

        if (time_now_z >= 1.2){
          time_now_z = 0;
          hit_ball_now = false ;
	        initial_pose[14] = new_pose[14];
        }
      }
      if(!keep_running){
		  std::cout << "stopping" << std::endl;
		  return franka::MotionFinished(new_pose);
	    }
	    std::cout<< "x= " <<x_desired_filtered<< ", y= " <<y_desired_filtered<< ", z= " << delta_z << std::endl ;
      return new_pose;
    });
  }
  catch (const franka::Exception& e) {
    std::cout << e.what() << std::endl;
    return -1;
  }
}

void coordinates_call_back(const pingpong_robot::Num::ConstPtr& msg) {
    
  double x_current;
  double y_current;
  double z_current;
     
  x_current = -msg->x ;
  y_current = 1 - msg->z ;//msg->y ;
  z_current = -msg->y ;//1 - msg->z ; // (1-) if the camera is positioned over the ball
    
  if (first_callback_loop ){
    x_last = x_current;
    y_last = y_current;
    z_threshold = z_current + 0.1 ;

    first_callback_loop =false;
  }

  if (z_current <= z_threshold && start_juggling){
    hit_ball_now = true;
  }
  if (time_since_last_message){
    {
      // update desired position in xy plane
      std::unique_lock<std::mutex> lock(xy_desired_mutex); 
      x_desired = x_current - x_last ;
      y_desired = y_current - y_last ;  
    } 
    x_last = x_current;
    y_last = y_current; 

    time_since_last_message = false ;
  }
}

void callback_timer_since_last_message(const ros::TimerEvent&)
{
  time_since_last_message = true ;
}

int main(int argc, char** argv) {
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " <robot-hostname>" << std::endl;
    return -1;
  }
  //initializing ros
  ros::init(argc, argv, "arm_motion_node");
  ros::NodeHandle n;
  ros::Timer timer1 = n.createTimer(ros::Duration(1.2), callback_timer_since_last_message);

  //initializing libfranka
  franka::Robot robot(argv[1]);
  franka::Gripper gripper(argv[1]);
  setDefaultBehavior(robot);

  // First move the robot to a suitable joint configuration
  //std::array<double, 7> q_goal = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
  std::array<double, 7> q_goal = {{M_PI_2, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI, -M_PI_4}};
  //std::array<double, 7> q_goal = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI, -M_PI_4}};

  MotionGenerator motion_generator(0.4, q_goal);
  std::cout << "WARNING: This example will move the robot! "
            << "Please make sure to have the user stop button at hand!" << std::endl
            << "Press Enter to continue..." << std::endl;
  std::cin.ignore();
  robot.control(motion_generator);
  std::cout << "Finished moving to initial joint configuration." << std::endl;

  // Set additional parameters always before the control loop,
  // NEVER in the control loop!
  // Set the joint impedance.
  robot.setJointImpedance({{3000, 3000, 3000, 2500, 2500, 2000, 2000}});
  // Set the collision behavior.
  std::array<double, 7> lower_torque_thresholds_nominal{
          {25.0, 25.0, 22.0, 20.0, 19.0, 17.0, 14.}};
  std::array<double, 7> upper_torque_thresholds_nominal{
          {35.0, 35.0, 32.0, 30.0, 29.0, 27.0, 24.0}};
  std::array<double, 6> upper_force_thresholds_nominal{
          {40.0, 40.0, 40.0, 35.0, 35.0, 35.0}};
  std::array<double, 6> lower_force_thresholds_nominal{
          {30.0, 30.0, 30.0, 25.0, 25.0, 25.0}};
  std::array<double, 7> lower_torque_thresholds_acceleration{
          {25.0, 25.0, 22.0, 20.0, 19.0, 17.0, 14.0}};
  std::array<double, 7> upper_torque_thresholds_acceleration{
          {35.0, 35.0, 32.0, 30.0, 29.0, 27.0, 24.0}};
  std::array<double, 6> lower_force_thresholds_acceleration{
          {30.0, 30.0, 30.0, 25.0, 25.0, 25.0}};
  std::array<double, 6> upper_force_thresholds_acceleration{
          {40.0, 40.0, 40.0, 35.0, 35.0, 35.0}};
  
  // std::array<double, 7> lower_torque_thresholds_acceleration{
  //         {25.0, 50.0, 22.0, 40.0, 19.0, 34.0, 14.0}};
  // std::array<double, 7> upper_torque_thresholds_acceleration{
  //         {35.0, 70.0, 32.0, 60.0, 29.0, 54.0, 24.0}};
  // std::array<double, 6> lower_force_thresholds_acceleration{
  //         {30.0, 30.0, 60.0, 25.0, 25.0, 50.0}};
  // std::array<double, 6> upper_force_thresholds_acceleration{
  //         {40.0, 40.0, 80.0, 35.0, 35.0, 70.0}};
  

  robot.setCollisionBehavior(
          lower_torque_thresholds_acceleration, upper_torque_thresholds_acceleration,
          lower_torque_thresholds_nominal, upper_torque_thresholds_nominal,
          lower_force_thresholds_acceleration, upper_force_thresholds_acceleration,
          lower_force_thresholds_nominal, upper_force_thresholds_nominal);

  /*std::cout << "Please position the bat in the gripper & press enter !" << std::endl;
  std::cin.ignore();
  gripper_grip(gripper);*/
  std::cout << "Please position the ball on the bat & press enter !" << std::endl;
  std::cin.ignore();
  ros::Subscriber sub = n.subscribe("coordinates", 1, coordinates_call_back);
  std::thread mythread = std::thread([&](){ //creating another thrad for the motion tracking
    motion(robot);
  });
  std::thread satrt_playing = std::thread([&](){ //creating another thread to start playing
      std::cout << "if you are ready to start playing press enter !" << std::endl;
      std::cin.ignore();
      start_juggling = true ;
  });
  keep_running = true;
  std::thread stopper_thread = std::thread([&](){
        std::cout << "Hit enter second time to stop" << std::endl;
	std::cin.ignore();
        //std::cerr << "stopping" << std::endl;
	keep_running = false;
        ros::shutdown();
                  //std::cerr << "stopping2" << std::endl;
	});

  if (!keep_running){
    gripper_releas(gripper);
    std::array<double, 7> f_goal = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
    MotionGenerator motion_generator(0.4, f_goal);
  }

  ros::spin();
  std::cerr << "joining threads" << std::endl;
  stopper_thread.join();
  mythread.join();
  return 0;
}

























// int hit_ball (franka::Robot & robot){
//   try{
//     double time = 0.0;
//     bool up = true;
//     std::array<double, 16> initial_pose; 
//     std::array<double, 16> new_pose;
//     double initial_z ,final_z ;


//     robot.control([&time , &new_pose, &up, &initial_pose, &initial_z, &final_z]
//                                     (const franka::RobotState& robot_state,
//                           franka::Duration period) -> franka::CartesianPose {
//       time += period.toSec();
//       if ( time == 0 ){//first_call_for_hit_ball_function) {
//         initial_pose = robot_state.O_T_EE_c;
//         new_pose = initial_pose; 
//         initial_z = initial_pose[14];
//         final_z = initial_z + 0.2 ; // hight to reach in z 20 cm

//         first_call_for_hit_ball_function = false;
//       }
//       else if ((new_pose[14] < final_z) && up ){
//         new_pose[14] += 0.001;
//         //ros::Duration(0.005).sleep(); // sleep for x in second
//       }
//       else if (new_pose[14] >= final_z){
//         up = false ;
//       }
//       else if (!up && (new_pose[14] > initial_z )){
//         new_pose[14] -= 0.001;
//         //ros::Duration(0.005).sleep(); // sleep for x in second
//       }
//       else {
//         return franka::MotionFinished(new_pose);
//         hit_ball_now = false;
//         }
      
//       return new_pose;
//     });

//   }
//   catch (const franka::Exception& e) {
//     std::cout << e.what() << std::endl;
//     return -1;
//   }
  
// }


/*


int motion_z (franka::Robot* robot){
  try {
	
      double v_z = 0.0;
      double time = 0.0;

      robot->control([=, &time,&v_z](const franka::RobotState&,
                               franka::Duration period) -> franka::CartesianVelocities {
      time += period.toSec();
        
    franka::CartesianVelocities output= {{0.0, 0.0, v_z, 0.0, 0.0, 0.0}};
        
    if (v_z <=0.6 && time <= 0.5)
	  {
	    v_z += 0.001;
	  }
	  else if (time >= 0.5)
	  {
	    v_z -= 0.0015;
	  }
	
	
	
	  if (v_z < 0)
	  {

      return franka::MotionFinished(output);
	  }

       
    return output;
    });
  } 

  catch (const franka::Exception& e) {
  std::cout << e.what() << std::endl;
  return -1;
  }
}

*/






















/*


// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#include <cmath>
#include <iostream>

#include <franka/exception.h>
#include <franka/robot.h>

#include "examples_common.h"

/**
 * @example generate_cartesian_pose_motion.cpp
 * An example showing how to generate a Cartesian motion.
 *
 * @warning Before executing this example, make sure there is enough space in front of the robot.
 


int motion_x (char** argv){
  try {
    franka::Robot robot(argv[1]);
    setDefaultBehavior(robot);

    // First move the robot to a suitable joint configuration
    std::array<double, 7> q_goal = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
    MotionGenerator motion_generator(0.5, q_goal);
    std::cout << "WARNING: This example will move the robot! "
              << "Please make sure to have the user stop button at hand!" << std::endl
              << "Press Enter to continue..." << std::endl;
    std::cin.ignore();
    robot.control(motion_generator);
    std::cout << "Finished moving to initial joint configuration." << std::endl;

    // Set additional parameters always before the control loop, NEVER in the control loop!
    // Set collision behavior.
    robot.setCollisionBehavior(
        {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},
        {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}});

    constexpr double kRadius = 0.3;
    std::array<double, 16> initial_pose;
    double time = 0.0;
    robot.control([&time, &initial_pose](const franka::RobotState& robot_state,
                                         franka::Duration period) -> franka::CartesianPose {
      time += period.toSec();

      if (time == 0.0) {
        initial_pose = robot_state.O_T_EE_c;
      }

      double angle = M_PI / 4 * (1 - std::cos(M_PI / 5.0 * time));
      double delta_x;
      //double delta_y;
      //double delta_z;
      
      
	
	
	delta_x = kRadius * std::sin(angle);
        //delta_y = kRadius * (std::cos(angle) - 1);
	//delta_z = kRadius * (std::cos(angle) - 1);

      

      std::array<double, 16> new_pose = initial_pose;
      new_pose[12] += delta_x;
      //new_pose[13] -= delta_y;
      //new_pose[14] += delta_z;

      if (time >= 10.0) {
        std::cout << std::endl << "Finished motion x" << std::endl;
        return franka::MotionFinished(new_pose);
      }
      return new_pose;
    });
    
  } catch (const franka::Exception& e) {
    std::cout << e.what() << std::endl;
    return -1;
  }
}


int motion_y (char** argv){
  try {
    franka::Robot robot(argv[1]);
    setDefaultBehavior(robot);

    // First move the robot to a suitable joint configuration
    std::array<double, 7> q_goal = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
    MotionGenerator motion_generator(0.5, q_goal);
    robot.control(motion_generator);
    std::cout << "Finished moving to initial joint configuration." << std::endl;

    // Set additional parameters always before the control loop, NEVER in the control loop!
    // Set collision behavior.
    robot.setCollisionBehavior(
        {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},
        {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}});

    constexpr double kRadius = 0.3;
    std::array<double, 16> initial_pose;
    double time = 0.0;
    robot.control([&time, &initial_pose](const franka::RobotState& robot_state,
                                         franka::Duration period) -> franka::CartesianPose {
      time += period.toSec();

      if (time == 0.0) {
        initial_pose = robot_state.O_T_EE_c;
      }

      double angle = M_PI / 4 * (1 - std::cos(M_PI / 5.0 * time));
      //double delta_x;
      double delta_y;
      //double delta_z;
      
      
	
	
	//delta_x = kRadius * std::sin(angle);
        delta_y = kRadius * (std::cos(angle) - 1);
	//delta_z = kRadius * (std::cos(angle) - 1);

      

      std::array<double, 16> new_pose = initial_pose;
      //new_pose[12] += delta_x;
      new_pose[13] -= delta_y;
      //new_pose[14] += delta_z;

      if (time >= 10.0) {
        std::cout << std::endl << "Finished motion y" << std::endl;
        return franka::MotionFinished(new_pose);
      }
      return new_pose;
    });
    
  } catch (const franka::Exception& e) {
    std::cout << e.what() << std::endl;
    return -1;
  }
}


int motion_z_pos (char** argv){
  try {
    franka::Robot robot(argv[1]);
    setDefaultBehavior(robot);

    // First move the robot to a suitable joint configuration
    std::array<double, 7> q_goal = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
    MotionGenerator motion_generator(0.5, q_goal);
    robot.control(motion_generator);
    std::cout << "Finished moving to initial joint configuration." << std::endl;

    // Set additional parameters always before the control loop, NEVER in the control loop!
    // Set collision behavior.
    robot.setCollisionBehavior(
        {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},
        {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}});

    constexpr double kRadius = 0.3;
    std::array<double, 16> initial_pose;
    double time = 0.0;
    robot.control([&time, &initial_pose](const franka::RobotState& robot_state,
                                         franka::Duration period) -> franka::CartesianPose {
      time += period.toSec();

      if (time == 0.0) {
        initial_pose = robot_state.O_T_EE_c;
      }

      double angle = M_PI / 4 * (1 - std::cos(M_PI / 5.0 * time));
      //double delta_x;
      //double delta_y;
      double delta_z;
      
      
	
	
	//delta_x = kRadius * std::sin(angle);
        //delta_y = kRadius * (std::cos(angle) - 1);
	delta_z = kRadius * (std::cos(angle) - 1);

      

      std::array<double, 16> new_pose = initial_pose;
      //new_pose[12] += delta_x;
      //new_pose[13] -= delta_y;
      new_pose[14] += delta_z;

      if (time >= 5.0) {
        std::cout << std::endl << "Finished motion z " << std::endl;
        return franka::MotionFinished(new_pose);
      }
      return new_pose;
    });
    
  } catch (const franka::Exception& e) {
    std::cout << e.what() << std::endl;
    return -1;
  }
}


int motion_z_neg (char** argv){
  try {
    franka::Robot robot(argv[1]);
    setDefaultBehavior(robot);
/*
    // First move the robot to a suitable joint configuration
    std::array<double, 7> q_goal = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
    MotionGenerator motion_generator(0.5, q_goal);
    robot.control(motion_generator);
    std::cout << "Finished moving to initial joint configuration." << std::endl;
*
    // Set additional parameters always before the control loop, NEVER in the control loop!
    // Set collision behavior.
    robot.setCollisionBehavior(
        {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},
        {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}});

    constexpr double kRadius = 0.3;
    std::array<double, 16> initial_pose;
    double time = 5.0;
    robot.control([&time, &initial_pose](const franka::RobotState& robot_state,
                                         franka::Duration period) -> franka::CartesianPose {
      time += period.toSec();

      if (time == 5.0) {
        initial_pose = robot_state.O_T_EE_c;
      }

      double angle = M_PI / 4 * (1 - std::cos(M_PI / 5.0 * time));
      //double delta_x;
      //double delta_y;
      double delta_z;
      
      
	
	
	//delta_x = kRadius * std::sin(angle);
        //delta_y = kRadius * (std::cos(angle) - 1);
	delta_z = kRadius * (std::cos(angle) - 1);

      

      std::array<double, 16> new_pose = initial_pose;
      //new_pose[12] += delta_x;
      //new_pose[13] -= delta_y;
      new_pose[14] += delta_z;

      if (time >= 10.0) {
        std::cout << std::endl << "Finished motion z " << std::endl;
        return franka::MotionFinished(new_pose);
      }
      return new_pose;
    });
    
  } catch (const franka::Exception& e) {
    std::cout << e.what() << std::endl;
    return -1;
  }
}




int main(int argc, char** argv) {
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " <robot-hostname>" << std::endl;
    return -1;
  }

  
  motion_x(&argv[0]);
  motion_z_pos(&argv[0]);
  motion_y(&argv[0]);
  motion_z_neg(&argv[0]);


  return 0;
}











// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#include <cmath>
#include <iostream>
#include <franka/exception.h>
#include <franka/robot.h>
#include "examples_common.h"
int main(int argc, char** argv) {
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " <robot-hostname>" << std::endl;
    return -1;
  }
  try {
    franka::Robot robot(argv[1]);
    setDefaultBehavior(robot);
    // First move the robot to a suitable joint configuration
    std::array<double, 7> q_goal = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
    MotionGenerator motion_generator(0.5, q_goal);
    std::cout << "WARNING: This example will move the robot! "
              << "Please make sure to have the user stop button at hand!" << std::endl
              << "Press Enter to continue..." << std::endl;
    std::cin.ignore();
    robot.control(motion_generator);
    std::cout << "Finished moving to initial joint configuration." << std::endl;
    // Set additional parameters always before the control loop, NEVER in the control loop!
    // Set collision behavior.
    robot.setCollisionBehavior(
        {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}}, {{20.0, 20.0, 18.0, 18.0, 16.0, 14.0, 12.0}},
        {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}},
        {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}}, {{20.0, 20.0, 20.0, 25.0, 25.0, 25.0}});
    std::array<double, 16> initial_pose;
    std::array<double, 2> initial_elbow;
    double time = 0.0;



    std::array<double, 7> point_1 = {{0, -M_PI_4, 0, -2 * M_PI_4, 0, M_PI_2, M_PI_4}};
    MotionGenerator motion_point_1(0.2, point_1);
    //robot.control(motion_point_1);

    for (int i = 0; i<3; i++)
	{
	  std::array<double, 7> point_2 = {{M_PI_2, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI, M_PI_4}};
   	  MotionGenerator motion_point2(1, point_2);
    	  robot.control(motion_point2);


 	  std::array<double, 7> point_3 = {{M_PI_2, (-M_PI_4+0.125), 0, (-3 * M_PI_4+0.5), 0,( M_PI-0.5), M_PI_4}};
   	  MotionGenerator motion_point3(1, point_3);
    	  robot.control(motion_point3);
	}






    std::array<double, 7> q_goal_2 = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
    MotionGenerator motion_generator_2(0.3, q_goal_2);
    robot.control(motion_generator_2);

    
  } catch (const franka::Exception& e) {
    std::cout << e.what() << std::endl;
    return -1;
  }
  return 0;
}

*/

