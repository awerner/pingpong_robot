#include "opencv2/opencv.hpp"
#include "cv-helpers.hpp"
#include "pingpong_robot/Num.h"

#include <ros/ros.h>
#include <librealsense2/rs.hpp> // Include RealSense Cross Platform API
#include <librealsense2/rsutil.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <mutex>
#include <thread>
#include <vector> 
#include <sstream>
#include <algorithm>
#include <cmath>


using namespace cv;
using namespace std;
using namespace rs2;

std::mutex mymutex;
std::mutex mymutex_2;

int thresh = 100;  //canny
int max_thresh = 255;
RNG rng(12345);

int x=0;
int y=0;
double last_dist=0;
int counter_pixel=0;
int counter_dist=0;
int const n=4; //number of values to average
const int inpWidth = 640;        // Width of network's input image
const int inpHeight = 480;       // Height of network's input image
const float WHRatio       = inpWidth / (float)inpHeight;

vector<vector<int> > coordinate;
vector<vector<int> > temprory_coordinate;  
std::vector<int> last_coordinate;
std::vector<double> avg_dist;

bool first_reading =true;
bool first_distance = true;
bool ball_detected = true;
bool keep_running;

static const std::string OPENCV_WINDOW = "Image window";
static const std::string HSV_WINDOW = "HSV window";
static const std::string THRESHOLD_WINDOW = "Threshold window";
static const std::string CONTOUR_WINDOW = "Contour window";
static const std::string CIRCLE_WINDOW = "Circle window";
static const std::string DEPTH_WINDOW = "Depth window";


void image_process(Mat frame)
{
  Mat canny_output,frame_HSV, frame_threshold,frame_gray,frame_bin;
  vector<vector<Point> > contours;
  vector<Vec4i> hierarchy;
  //cv::namedWindow(OPENCV_WINDOW);
  int  low_H = 164 , low_S = 155 , low_V = 54;
  int high_H = 179, high_S = 255, high_V = 191;
    
  last_coordinate.resize(2); 
	avg_dist.resize(n); 
	  

    
  cvtColor(frame, frame_HSV, COLOR_BGR2HSV);    //Convert from BGR to HSV colorspace
  inRange(frame_HSV, Scalar(low_H, low_S, low_V), Scalar(high_H, high_S, high_V),
               frame_threshold); // Detect the object based on HSV Range Values
  cvtColor( frame, frame_gray, CV_BGR2GRAY ); /// Convert it to gray
  GaussianBlur( frame_threshold, frame_gray, Size(9, 9), 2, 2 ); /// Reduce the noise so we avoid false circle detection
  vector<Vec3f> circles;
  /*
  /// Apply the Hough Transform to find the circles
  HoughCircles( frame_gray, circles, CV_HOUGH_GRADIENT, 1, frame_gray.rows/8, 200, 100, 0, 0 );

  // Draw the circles detected
  for( size_t i = 0; i < circles.size(); i++ )
  {
    Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
    int radius = cvRound(circles[i][2]);
    // circle center
    circle( frame, center, 3, Scalar(0,255,0), -1, 8, 0 );
    // circle outline
    circle( frame, center, radius, Scalar(0,0,255), 3, 8, 0 );
  }
  */    

  Canny( frame_threshold, canny_output, thresh, thresh*2, 3 ); /// Detect edges using canny
  findContours( canny_output, contours, hierarchy, CV_RETR_TREE,
                 CV_CHAIN_APPROX_SIMPLE,Point(0, 0) ); /// Find contours
  Mat drawing = Mat::zeros( canny_output.size(), CV_8UC3 );/// Draw contours
  for( int i = 0; i< contours.size(); i++ ){
    Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
    drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, Point() );
  }
  cvtColor( drawing, drawing, CV_BGR2GRAY );
  threshold( drawing, frame_bin, 100,255,THRESH_BINARY ); // convert grayscale to binary image
  Moments m = moments(frame_bin,true);// find moments of the image
  Point p(m.m10/m.m00, m.m01/m.m00);  // coordinates of centroid
  //cout << p.x <<" "<<p.y <<endl;
  if (p.x == -2147483648 && p.y == -2147483648){
    ball_detected = false;
  }
  else ball_detected = true;
  if (p.x >=0 && p.x <=1000) { //neglect large numbers sent
    if (first_reading) {
      last_coordinate.at(0) = p.x;
      last_coordinate.at(1) = p.y;
      first_reading = false;
    }
    if (abs(p.x-last_coordinate.at(0)) < 150 && 
          abs(p.y-last_coordinate.at(1)) < 150) {
      coordinate.insert(coordinate.begin(),std::vector<int>{p.x,p.y});
      if (coordinate.size() == n-1) {
        //std::sort(&avg_x.at(0),&avg_x.at(n-1));
        //std::sort(&avg_y.at(0),&avg_y.at(n-1));
        //for(auto const & x : coordinate) {
        //  std::cout << "H" << x.at(0) << " " << x.at(1) << std::endl;
        //}
        
        temprory_coordinate.assign(coordinate.begin(), (coordinate.end()-1)); 
        std::sort(coordinate.begin(),coordinate.end(),
              [](std::vector<int> const & a, std::vector<int> const &b){
          if (a.at(0)>b.at(0)) return true;
          if (a.at(0)<b.at(0)) return false;
          return a.at(1)>b.at(1);
        });
        //for(auto const & x : coordinate) {
        //  std::cout << "L" << x.at(0) << " " << x.at(1) << std::endl;
        //}
        {
          std::unique_lock<std::mutex> lock(mymutex);
          x = coordinate.at(n/2).at(0);
          y = coordinate.at(n/2).at(1);
          //std::cout <<"coordinates {"<< x<< ", " << y <<"}"<<std::endl;
        }
        coordinate.pop_back();
        coordinate.assign(temprory_coordinate.begin(), temprory_coordinate.end());
        temprory_coordinate.clear();
	    }
	    last_coordinate.at(0)=p.x;
	    last_coordinate.at(1)=p.y;
	  } 
    else if(last_coordinate.at(0)==0 && last_coordinate.at(1)==0){
      coordinate.push_back(std::vector<int>{p.x,p.y});
      last_coordinate.at(0)=p.x;
      last_coordinate.at(1)=p.y;
    }
    else {
        //cout << "coordinate reading cancelled"<<endl;
      }
	} 

  circle(frame, p, 5, Scalar(128,0,0), -1);
    
  //cv::SimpleBlobDetector detector;
  SimpleBlobDetector::Params params;
  std::vector<KeyPoint> keypoints;

  // Change thresholds
  params.minThreshold = 10;
  params.maxThreshold = 300;
    
  // Filter by Area.
  params.filterByArea = true;
  params.minArea = 50;
    
  // Filter by Circularity
  params.filterByCircularity = true;
  params.minCircularity = 0.5;
    
  // Filter by Convexity
  params.filterByConvexity = true;
  params.minConvexity = 0.2;
    
  // Filter by Inertia
  params.filterByInertia = true;
  params.minInertiaRatio = 0.1;
  cv::Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create(params);
    
  detector->detect( frame, keypoints );
  Mat im_with_keypoints;
  cv::drawKeypoints( frame, keypoints, im_with_keypoints, Scalar(0,0,255),
                       DrawMatchesFlags::DRAW_RICH_KEYPOINTS ); 

   


  // Update GUI Window
  //cv::imshow(OPENCV_WINDOW,frame );
  //cv::imshow(HSV_WINDOW, frame_HSV);
  //cv::imshow(THRESHOLD_WINDOW, frame_threshold);
  //cv::imshow(CONTOUR_WINDOW, canny_output );
  //cv::imshow(CIRCLE_WINDOW, im_with_keypoints );
  
  //cv::waitKey(3);  
}

void calculate_distances (const struct rs2_intrinsics * intrin, Mat depth_mat,
                                                 float (&point_3d)[3] ){
  int u,v;
  float distance;
  
  float Desired_Pixel[2]; //  pixel
  float Return_Point[3]; //  point (in 3D)
  {
    std::unique_lock<std::mutex> lock(mymutex);
    u = x;
    v = y;
  }
  Point p (u,v);
  distance = depth_mat.at<double>(p);
  
  Desired_Pixel[0] = p.x;
  Desired_Pixel[1] = p.y;

  rs2_deproject_pixel_to_point(Return_Point, intrin, Desired_Pixel, distance);
  cout <<"x= "<< Return_Point[0] <<" ,y= "<< Return_Point[1] <<", z= "<< Return_Point[2] <<endl;
  std::copy(std::begin(Return_Point), std::end(Return_Point), std::begin(point_3d));

}


int main(int argc, char * argv[]) try
{
  ros::init(argc, argv, "image_proccessing_node");
  ros::NodeHandle nh;
  ros::Publisher coordinates_pub_ = nh.advertise<pingpong_robot::Num>("coordinates", 100);
  int itiration_number = 0 ;
  float point_3d [3];

  rs2::pipeline pipe;
  auto config = pipe.start();
  auto profile = config.get_stream(RS2_STREAM_COLOR)
                     .as<video_stream_profile>();
  rs2::align align_to(RS2_STREAM_COLOR);

  auto stream = config.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>();
  auto intrinsics = stream.get_intrinsics(); // Calibration data

  //Crop image to network's input size
  Size cropSize;
  if (profile.width() / (float)profile.height() > WHRatio){
    cropSize = Size(static_cast<int>(profile.height() * WHRatio),profile.height());
  }
  else{
    cropSize = Size(profile.width(),static_cast<int>(profile.width() / WHRatio));
  }

  Rect crop(Point((profile.width() - cropSize.width) / 2,
                  (profile.height() - cropSize.height) / 2),
                  cropSize);
  keep_running = true;
  std::thread stopper_thread = std::thread([&](){
        std::cout << "Hit enter to stop" << std::endl;
	std::cin.ignore();
       //std::cerr << "stopping" << std::endl;
	keep_running = false;
        ros::shutdown();
                  //std::cerr << "stopping2" << std::endl;
	});

  //Window to show detections
  const auto window_name = "Display Image";
  namedWindow(window_name, WINDOW_AUTOSIZE);

  while (getWindowProperty(window_name, WND_PROP_AUTOSIZE) >= 0){
    // Wait for the next set of frames
    auto data = pipe.wait_for_frames();
    // Make sure the frames are spatially aligned
    data = align_to.process(data);

    auto color_frame = data.get_color_frame();
    auto depth_frame = data.get_depth_frame();

    // If we only received new depth frame,
    // but the color did not update, continue
    static int last_frame_number = 0;
    if (color_frame.get_frame_number() == last_frame_number) continue;
    last_frame_number = color_frame.get_frame_number();

    // Convert RealSense frame to OpenCV matrix:
    auto color_mat = frame_to_mat(color_frame);
    auto depth_mat = depth_frame_to_meters(pipe, depth_frame);

    image_process(color_mat);
    if (ball_detected){
      calculate_distances(&intrinsics,depth_mat, point_3d);
      if (itiration_number<5) itiration_number++ ;
      //publish msg 
      if (itiration_number >= 4 ){
        pingpong_robot::Num msg;
		    msg.x =   point_3d [0] ;
		    msg.y =   point_3d [1] ;
		    msg.z =   point_3d [2] ;
        if (msg.x !=0.0 && msg.y !=0.0 && msg.z !=0.0){
	        if (first_distance){
            last_dist = msg.z;
            coordinates_pub_.publish(msg);
            first_distance = false;
          }
          else if (!first_distance && abs(last_dist-msg.z) <= 0.3){
            coordinates_pub_.publish(msg);
          }
        }
      }
    }
    imshow(window_name, color_mat);
    if (waitKey(1) >= 0) break;

    if(!keep_running){
		  std::cout << "stopping" << std::endl;
		  return EXIT_SUCCESS;
	  }    

  }  
  return EXIT_SUCCESS;
  stopper_thread.join();

}
catch (const rs2::error & e)
{
    std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
    return EXIT_FAILURE;
}
catch (const std::exception& e)
{
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}
