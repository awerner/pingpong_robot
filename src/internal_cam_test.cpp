#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <sensor_msgs/PointCloud.h>
#include <pcl/point_types.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl_ros/transforms.h>
#include <geometry_msgs/Point.h>
#include <boost/foreach.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/String.h"
#include "std_msgs/Int32.h"
#include "pingpong_robot/Num.h"
#include <librealsense2/rs.hpp> // Include RealSense Cross Platform API
#include <librealsense2/rsutil.h>
//#include "example.hpp"


#include <iostream>
#include <sstream>
#include <vector> 
#include <algorithm>
#include <cmath>
#include <mutex>
//libfranka/include
//#include <franka/exception.h>
//#include <franka/robot.h>
//#include <franka/gripper.h>


using namespace cv;
using namespace std;


typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
//sensor_msgs::PointCloud2 depth;

int thresh = 100;  //canny
int max_thresh = 255;
RNG rng(12345);

std::mutex mymutex;
std::mutex mymutex_2;

int x=0;
int y=0;
double last_dist=0;
int counter_pixel=0;
int counter_dist=0;
int const n=4; //number of values to average

vector<vector<int> > coordinate ( n , vector<int> (2, 0));  
std::vector<int> last_coordinate;
std::vector<double> avg_dist;
bool first_reading =true;
bool pCloud_frame = false;

float X ;
float Y ;
float Z ;



static const std::string OPENCV_WINDOW = "Image window";
static const std::string HSV_WINDOW = "HSV window";
static const std::string THRESHOLD_WINDOW = "Threshold window";
static const std::string CONTOUR_WINDOW = "Contour window";
static const std::string CIRCLE_WINDOW = "Circle window";
static const std::string DEPTH_WINDOW = "Depth window";




class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Subscriber depth_sub_;

  ros::Publisher coordinates_pub_;

  double distance =0;

  public:
    

    int  low_H = 164 , low_S = 155 , low_V = 54;
    int high_H = 179, high_S = 255, high_V = 191;
  

    void imageCb(const sensor_msgs::ImageConstPtr& msg);
    void imagedp(const sensor_msgs::ImageConstPtr& msg);

    void distance_calc(Mat frame_depth);
    void image_proc(Mat frame_depth);


    ImageConverter()
      : it_(nh_)
    {
      // Subscribe to input video feed and publish output
      //depth_sub_ = it_.subscribe("/camera/depth/image_rect_raw", 1, &ImageConverter::imagedp, this);
      image_sub_ = it_.subscribe("/camera/color/image_raw", 1, &ImageConverter::imageCb, this);
      //coordinates_pub_ = nh_.advertise<pingpong_robot::Num>("coordinates", 100);


      cv::namedWindow(OPENCV_WINDOW);
      /*cv::namedWindow(HSV_WINDOW);
      cv::namedWindow(THRESHOLD_WINDOW);
      cv::namedWindow(CONTOUR_WINDOW);
      cv::namedWindow(CIRCLE_WINDOW);
      cv::namedWindow(DEPTH_WINDOW);*/
   
  	  last_coordinate.resize(2); 
	    avg_dist.resize(n); 
    }

    ~ImageConverter()
    {
      cv::destroyWindow(OPENCV_WINDOW);
      cv::destroyWindow(HSV_WINDOW);
      cv::destroyWindow(THRESHOLD_WINDOW);
      cv::destroyWindow(CONTOUR_WINDOW);
      cv::destroyWindow(CIRCLE_WINDOW);
      cv::destroyWindow(DEPTH_WINDOW);
    }


};



void ImageConverter::distance_calc(Mat frame_depth)
{
    
    double distance;
  {
	  std::unique_lock<std::mutex> lock(mymutex);
    distance = 0.001*frame_depth.at<u_int16_t>(x, y);
  }

    avg_dist.at(counter_dist) = distance;
    counter_dist ++;

    if (counter_dist == n-1)
    {
	    std::sort(&avg_dist.at(0),&avg_dist.at(n-1));

	    if (first_reading)
	    {
		    distance= avg_dist.at(n/2);
		    last_dist = distance;
		    first_reading = false; 
	    }
	    else if (abs(avg_dist.at(n/2)-last_dist) <=0.1 && avg_dist.at(n/2) !=0 )
	    {
		    distance = avg_dist.at(n/2);
		    last_dist = distance;
	    }
	    else{
        //cout<< "distance reading cancelled" <<endl;
      }
      /*
      // librealsense function
      float upixel[2]; // From pixel
      float upoint[3]; // 3d point
      {
        std::unique_lock<std::mutex> lock(mymutex);
        upixel[0] = x;
        upixel[1] = y;
      }
      
      rs2::pipeline pipe;
      rs2::config cfg;
      cfg.enable_stream(RS2_STREAM_COLOR, 640, 480, RS2_FORMAT_BGR8, 30);
      //pipe.start(cfg);
      //rs2::pipeline_profile pipeProfile = pipe.start(cfg);
      ros::Duration(5).sleep(); // sleep for x in seconds

      //rs2_intrinsics intr = pipeProfile.get_profile().as<rs2::video_stream_profile>().get_intrinsics(); // Calibration data
      rs2_intrinsics intr = pipeProfile.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>().get_intrinsics();

      rs2_deproject_pixel_to_point(upoint, &intr, upixel, distance);
      */
	    cout << "Distance to ball =" << distance << endl<<endl;
	    //cout << ros::Time::now() <<endl;


	    counter_dist=0;
    }

    cv::imshow(DEPTH_WINDOW, frame_depth );
    cv::waitKey(3);

}



void ImageConverter::image_proc(Mat frame)
{
    Mat canny_output,frame_HSV, frame_threshold,frame_gray,frame_bin;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    
    cvtColor(frame, frame_HSV, COLOR_BGR2HSV);    //Convert from BGR to HSV colorspace
    inRange(frame_HSV, Scalar(low_H, low_S, low_V), Scalar(high_H, high_S, high_V), frame_threshold); // Detect the object based on HSV Range Values
    cvtColor( frame, frame_gray, CV_BGR2GRAY ); /// Convert it to gray
    GaussianBlur( frame_threshold, frame_gray, Size(9, 9), 2, 2 ); /// Reduce the noise so we avoid false circle detection
    vector<Vec3f> circles;
    /*
    /// Apply the Hough Transform to find the circles
    HoughCircles( frame_gray, circles, CV_HOUGH_GRADIENT, 1, frame_gray.rows/8, 200, 100, 0, 0 );

    // Draw the circles detected
    for( size_t i = 0; i < circles.size(); i++ )
    {
      Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
      int radius = cvRound(circles[i][2]);
      // circle center
      circle( frame, center, 3, Scalar(0,255,0), -1, 8, 0 );
      // circle outline
      circle( frame, center, radius, Scalar(0,0,255), 3, 8, 0 );
    }
    */    

    Canny( frame_threshold, canny_output, thresh, thresh*2, 3 ); /// Detect edges using canny
    findContours( canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) ); /// Find contours
    Mat drawing = Mat::zeros( canny_output.size(), CV_8UC3 );/// Draw contours
    for( int i = 0; i< contours.size(); i++ ){
      Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
      drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, Point() );
    }
    cvtColor( drawing, drawing, CV_BGR2GRAY );
    threshold( drawing, frame_bin, 100,255,THRESH_BINARY ); // convert grayscale to binary image
    Moments m = moments(frame_bin,true);// find moments of the image
    Point p(m.m10/m.m00, m.m01/m.m00);  // coordinates of centroid

   if (p.x >=0 && p.x <=1000){ //neglect large numbers sent
	  if (abs(p.x-last_coordinate[0]) < 150 && abs(p.y-last_coordinate[1]) < 150) {
	    coordinate[counter_pixel][0]= p.x;
	    coordinate[counter_pixel][1]= p.y;
	    if (counter_pixel == n-1){
	      //std::sort(&avg_x[0],&avg_x[n-1]);
	      //std::sort(&avg_y[0],&avg_y[n-1]);
	      std::sort(coordinate[0].begin(),coordinate[0].end());
		    {
		      std::unique_lock<std::mutex> lock(mymutex);
	        x= (int)coordinate[n/2][0];
	        y= (int)coordinate[n/2][1];
          //cout <<"coordinates {"<< x<< ", " << y <<"}"<<endl;
		    }
	      
	      counter_pixel=0;
	    }
	    last_coordinate[0]=p.x;
	    last_coordinate[1]=p.y;
	  }

    else if(last_coordinate[0]==0 && last_coordinate[1]==0){
	    coordinate[counter_pixel][0]= p.x;
	    coordinate[counter_pixel][1]= p.y;
	    last_coordinate[0]=p.x;
	    last_coordinate[1]=p.y;
    }
	  else {
      //cout << "coordinate reading cancelled"<<endl;
    }
    counter_pixel ++;
	}    
    circle(frame, p, 5, Scalar(128,0,0), -1);
    
    //cv::SimpleBlobDetector detector;
    SimpleBlobDetector::Params params;
    std::vector<KeyPoint> keypoints;

    // Change thresholds
    params.minThreshold = 10;
    params.maxThreshold = 300;
    
    // Filter by Area.
    params.filterByArea = true;
    params.minArea = 150;
    
    // Filter by Circularity
    params.filterByCircularity = true;
    params.minCircularity = 0.8;
    
    // Filter by Convexity
    params.filterByConvexity = true;
    params.minConvexity = 0.6;
    
    // Filter by Inertia
    params.filterByInertia = true;
    params.minInertiaRatio = 0.1;
    cv::Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create();
    
    detector->detect( canny_output, keypoints );
    Mat im_with_keypoints;
    cv::drawKeypoints( frame, keypoints, im_with_keypoints, Scalar(0,0,255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS ); 
    pingpong_robot::Num msg;
    if (pCloud_frame){
	    {
			  std::unique_lock<std::mutex> lock(mymutex_2);
		    msg.x =   X ;
		    msg.y =   Y ;
		    msg.z =   Z ;
      }
      pCloud_frame =false;
    }
    
	  coordinates_pub_.publish(msg);



  // Update GUI Window
  cv::imshow(OPENCV_WINDOW,frame );
  /*cv::imshow(HSV_WINDOW, frame_HSV);
  cv::imshow(THRESHOLD_WINDOW, frame_threshold);
  cv::imshow(CONTOUR_WINDOW, canny_output );
  cv::imshow(CIRCLE_WINDOW, im_with_keypoints );
  */
  cv::waitKey(3);  
}


void ImageConverter::imagedp(const sensor_msgs::ImageConstPtr& msg)
{
	cv_bridge::CvImagePtr cv_ptr;
	try
	{
		cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_16UC1);
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}
	distance_calc(cv_ptr->image);
}


void ImageConverter::imageCb(const sensor_msgs::ImageConstPtr& msg)
{
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    
    image_proc(cv_ptr->image);
}



void depthcallback (const sensor_msgs::PointCloud2 my_pcl)//const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{   
    //sensor_msgs::PointCloud2 my_pcl;
    //my_pcl = *cloud_msg;
    int u ,v;
    int width = my_pcl.width;
    int height = my_pcl.height;
    //cout << width<<"  ,"<<height<<endl;
    {
      std::unique_lock<std::mutex> lock(mymutex);
        u = y ;
        v = x ;
    }
    
    //int arrayPosition = v*my_pcl.row_step + u*my_pcl.point_step;
    int arrayPosition = (( 100 * 640 ) + 100) * width / 307200 ;
    cout << width<<"  ,"<<arrayPosition<< " , row="<< my_pcl.row_step << " ,point="<< my_pcl.point_step <<endl;
    int arrayPosX = arrayPosition + my_pcl.fields[0].offset; // X has an offset of 0
    int arrayPosY = arrayPosition + my_pcl.fields[1].offset; // Y has an offset of 4
    int arrayPosZ = arrayPosition + my_pcl.fields[2].offset; // Z has an offset of 8
    
    {
      std::unique_lock<std::mutex> lock(mymutex_2);
      memcpy(&X, &my_pcl.data[arrayPosX], sizeof(float));
      memcpy(&Y, &my_pcl.data[arrayPosY], sizeof(float));
      memcpy(&Z, &my_pcl.data[arrayPosZ], sizeof(float));
      X*=100;
      Y*=100;
      Z*=100;
      //cout <<"x= "<< X << ", y= " << Y <<", z ="<< Z <<endl;
    }
    pCloud_frame = true;
    

}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_converter");
  ros::NodeHandle nh;
  
  ImageConverter ic;
  ros::Subscriber cloud_sub_= nh.subscribe<sensor_msgs::PointCloud2>("/camera/depth/color/points", 1, depthcallback);
  ros::spin();
  
  
  return 0;
}








