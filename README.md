**This project aims at getting the panda robot to juggle a ping pong ball**

    1- for position detection i use intelrealsense D435 with the librealsense to get the RGB frames 
    and using opencv to get the ball position with color detection and blob detection then also using
    the librealsense to calculate the distance to the ball then getting the exact coordinates is space.
	(coordinates in camera_node are according to the camera's prespective)
    
    2- the coordinates are then sent to the arm_node from the camera_node through a ros message. 
    
    3- using the linfranka to move the Panda robotic arm to the ball position and hit it.
	(coordinates in arm_node are according to the Panda's designation)
